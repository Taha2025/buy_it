import 'package:flutter_app09/constants.dart';
import 'package:flutter_app09/models/product.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class Store {
  var loadProducts;
  final _firestore = FirebaseFirestore.instance;

  addProduct(Product product) {
    _firestore.collection(kProductsCollection).add({
      kProductName: product.pName,
      kProductDescription: product.pDescription,
      kProductLocation: product.pLocation,
      kProductCategory: product.pCategory,
      kProductPrice: product.pPrice
    }).then((value) => print("$value")).onError((error, stackTrace) => print("$error"));
  }
}

Stream<QuerySnapshot> loadProducts() {
  var _firestore;
  return _firestore.collection(kProductsCollection).snapshots();
}




