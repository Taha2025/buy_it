import 'package:flutter_app09/constants.dart';
import 'package:flutter_app09/screens/admin/addProduct.dart';
import 'package:flutter_app09/screens/admin/editProduct.dart';
import 'package:flutter/material.dart';

class AdminHome extends StatelessWidget {
  static String id='AdminHome';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kMainColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: double.infinity,
          ),
          ElevatedButton(
            onPressed: ()
            {
              Navigator.pushNamed(context, AddProduct.id);
            },
            child: Text('Add Product')
            ,
          ),
          ElevatedButton(
            onPressed: ()
            {
              Navigator.pushNamed(context, EditProduct.id);
            },
            child: Text('Edit Product')
            ,
          ),
          ElevatedButton(
            onPressed: (){},
            child: Text('View orders')
            ,
          )
        ],
      ),
    );
  }
}