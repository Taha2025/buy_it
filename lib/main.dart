import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_app09/provider/adminMode.dart';
import 'package:flutter_app09/provider/modelHud.dart';
import 'package:flutter_app09/screens/admin/addProduct.dart';
import 'package:flutter_app09/screens/admin/adminHome.dart';
import 'package:flutter_app09/screens/admin/editProduct.dart';
import 'package:flutter_app09/screens/login_screen.dart';
import 'package:flutter_app09/screens/signup_screen.dart';
import 'package:flutter_app09/screens/user/homePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  MultiProvider(
      providers:
      [
        ChangeNotifierProvider<ModelHud>(
          create: (context)=>ModelHud(),),
        ChangeNotifierProvider<AdminMode>(
          create: (context)=>AdminMode(),)
      ],
      child: MaterialApp(
        initialRoute: LoginScreen.id,
        routes:
        {
          LoginScreen.id:(context)=>LoginScreen(),
          SignupScreen.id:(context)=>SignupScreen(),
          HomePage.id: (context) =>HomePage(),
          AdminHome.id:(context)=>AdminHome(),
          AddProduct.id:(context) =>AddProduct(),
          EditProduct.id:(context) =>EditProduct()
        },
      ),
    );

  }
}